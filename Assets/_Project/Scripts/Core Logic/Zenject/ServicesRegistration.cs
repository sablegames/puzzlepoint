using _Project.Scripts.Features.Loading;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.SaveSystem;
using _Project.Scripts.Services.Timer;
using _Project.Scripts.Services.Timer.Interfaces;
using Zenject;

namespace _Project.Scripts.Core_Logic.Zenject
{
  public class ServicesRegistration : MonoInstaller
  {
    public override void InstallBindings()
    {
      BindSingleService<IAssetProvider, AssetProvider>();
      BindSingleService<IAudioService, AudioService>();
      BindSingleService<IGameSaveSystem, GameSaveSystem>();
      BindSingleService<IGameTimerService, GameTimerService>();
      BindSingleService<ILoadingService, LoadingService>();
    }

    private void BindSingleService<T, T2>() where T2 : T => 
      Container.Bind<T>().To<T2>().AsSingle();
  }
}