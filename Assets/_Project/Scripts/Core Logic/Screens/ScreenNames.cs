namespace _Project.Scripts.Core_Logic.Screens
{
  public static class ScreenNames
  {
    public const string Splash = "splash";
    public const string Privacy = "privacy";
    public const string Home = "home";
    public const string Classic = "classic";
    public const string Shop = "shop";
    public const string Game = "game";
    public const string Result = "result";
  }
}