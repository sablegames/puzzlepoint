using Cysharp.Threading.Tasks;

namespace _Project.Scripts.Core_Logic.StatesMachine.Interfaces
{
  public interface IGameStateMachine
  {
    void Enter<TState>() where TState : class, IState;
    void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload>;
    UniTask AsyncEnter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload>;
    UniTask AsyncEnter<TState>() where TState : class, IState;
  }
}