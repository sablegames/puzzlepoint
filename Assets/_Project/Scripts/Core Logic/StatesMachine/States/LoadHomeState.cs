using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Features.HomeScreen;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.SaveSystem;
using _Project.Scripts.Services.Timer.Interfaces;
using Cysharp.Threading.Tasks;

namespace _Project.Scripts.Core_Logic.StatesMachine.States
{
  public class LoadHomeState : IState
  {
    private readonly IAudioService _audioService;
    private readonly IAssetProvider _assetProvider;
    private readonly IGameTimerService _gameTimerService;

    private IGameStateMachine _gameStateMachine;

    private HomeLogic _homeLogic;

    public LoadHomeState(IAssetProvider assetProvider, IAudioService audioService, IGameTimerService gameTimerService)
    {
      _audioService = audioService;
      _gameTimerService = gameTimerService;
      _assetProvider = assetProvider;
    }

    public void Construct(IGameStateMachine gameStateMachine) =>
      _gameStateMachine = gameStateMachine;

    public void Enter()
    {
      _homeLogic = new HomeLogic(_assetProvider, _gameTimerService, _audioService);
      _homeLogic.Enter();
    }

    public UniTask EnterAsync() => 
      throw new System.NotImplementedException();

    public void Exit() =>
      _homeLogic.CloseHomeView();
  }
}