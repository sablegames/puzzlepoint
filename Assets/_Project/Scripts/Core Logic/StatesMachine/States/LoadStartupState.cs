using System;
using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Features.Loading;
using _Project.Scripts.Features.Splashscreen;
using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.SaveSystem;
using Cysharp.Threading.Tasks;

namespace _Project.Scripts.Core_Logic.StatesMachine.States
{
  public class LoadStartupState : IState
  {
    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    private readonly IGameSaveSystem _gameSaveSystem;
    private readonly ILoadingService _loadingService;

    private IGameStateMachine _gameStateMachine;
    private SplashLogic _splashLogic;

    public LoadStartupState(IAudioService audioService, IGameSaveSystem gameSaveSystem, IAssetProvider assetProvider, ILoadingService loadingService)
    {
      _assetProvider = assetProvider;
      _audioService = audioService;
      _gameSaveSystem = gameSaveSystem;
      _loadingService = loadingService;
    }

    public void Construct(IGameStateMachine gameStateMachine) =>
      _gameStateMachine = gameStateMachine;

    public void Enter()
    {
      LaunchWithSplash(LoadPolicy);
      LoadSound();
    }

    public UniTask EnterAsync() =>
      throw new NotImplementedException();

    public void Exit()
    {
    }

    private void LaunchWithSplash(Action onSplashComplete)
    {
      _splashLogic = new SplashLogic(_assetProvider);
      _splashLogic.Launch(onSplashComplete);

      _splashLogic.Complete();
    }

    private void LoadPolicy()
    {
      _loadingService.Enter(() =>
      {
        _splashLogic.Clear();
        _gameStateMachine.Enter<LoadHomeState>();
      });
    }

    private void LoadSound()
    {
      _audioService.MuteMusic(_gameSaveSystem.Get().IsMusicDisabled);
      _audioService.MuteSounds(_gameSaveSystem.Get().IsSoundDisabled);
    }
  }
}