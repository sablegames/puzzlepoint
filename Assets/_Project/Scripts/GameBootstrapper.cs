using _Project.Scripts.Core_Logic.StatesMachine.Interfaces;
using _Project.Scripts.Core_Logic.StatesMachine.States;
using UnityEngine;
using Zenject;

namespace _Project.Scripts
{
  public class GameBootstrapper : MonoBehaviour
  {
    private IGameStateMachine _gameStateMachine;

    private void Awake()
    {
      Application.targetFrameRate = 60;

      if (!Application.isEditor)
        Debug.unityLogger.logEnabled = false;

      LaunchStartupState();
    }

    [Inject]
    public void Construct(IGameStateMachine gameStateMachine) =>
      _gameStateMachine = gameStateMachine;


    private void LaunchStartupState() =>
      _gameStateMachine.Enter<LoadStartupState>();
  }
}