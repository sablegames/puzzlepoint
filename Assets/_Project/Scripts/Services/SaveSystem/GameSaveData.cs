using System.Collections.Generic;
using _Project.Scripts.Services.Timer.Data;

namespace _Project.Scripts.Services.SaveSystem
{
  public class GameSaveData
  {
    public bool IsMusicDisabled;
    public bool IsSoundDisabled;
    
    public List<GameTimerData> GameTimers = new();
  }
}