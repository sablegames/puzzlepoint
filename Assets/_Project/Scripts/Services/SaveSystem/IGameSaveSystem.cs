using _Project.Scripts.Service_Base;

namespace _Project.Scripts.Services.SaveSystem
{
  public interface IGameSaveSystem : IService
  {
    GameSaveData Get();
    void Save();
  }
}