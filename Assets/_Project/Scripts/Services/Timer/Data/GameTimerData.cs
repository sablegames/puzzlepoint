using System;

namespace _Project.Scripts.Services.Timer.Data
{
  [Serializable]
  public struct GameTimerData
  {
    public string UniqueId;
    public string StartTime; //DateTime
    public uint RemainTime;
  }
}