using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using _Project.Scripts.Services.SaveSystem;
using _Project.Scripts.Services.Timer.Data;
using _Project.Scripts.Services.Timer.Interfaces;

namespace _Project.Scripts.Services.Timer
{
  public class GameTimerService : IGameTimerService
  {
    private readonly IGameSaveSystem _gameSaveSystem;

    private readonly Dictionary<string, IGameTimer> _gameTimers = new();

    private bool _isInitialize;

    public GameTimerService(IGameSaveSystem gameSaveSystem) => 
      _gameSaveSystem = gameSaveSystem;

    public void Initialize()
    {
      if (_isInitialize)
        return;
      
      foreach (GameTimerData gameTimerData in _gameSaveSystem.Get().GameTimers)
        _gameTimers.Add(gameTimerData.UniqueId, new GameTimer(gameTimerData, this));

      _isInitialize = true;
    }

    public IGameTimer CreateTimer(TimeSpan duration, string guid = "")
    {
      var gameTimerData = new GameTimerData()
      {
        UniqueId = guid == "" ? Guid.NewGuid().ToString() : guid,
        RemainTime = (uint)duration.TotalSeconds,
        StartTime = DateTime.Now.ToString(CultureInfo.InvariantCulture)
      };

      var gameTimer = new GameTimer(gameTimerData, this);
      _gameTimers.Add(gameTimerData.UniqueId, gameTimer);

      _gameSaveSystem.Get().GameTimers.Add(gameTimerData);
      _gameSaveSystem.Save();

      return gameTimer;
    }

    public IGameTimer GetTimer(string id) =>
      _gameTimers[id];

    public bool IsTimerExist(string id) =>
      _gameTimers.ContainsKey(id);

    public void DestroyTimer(string id)
    {
      GameTimerData dataForDelete = _gameSaveSystem.Get().GameTimers.FirstOrDefault(p => p.UniqueId == id);
      _gameSaveSystem.Get().GameTimers.Remove(dataForDelete); //Delete timer data
      _gameSaveSystem.Save();

      _gameTimers[id].KillTimerProcess();
      _gameTimers.Remove(id);
    }
  }
}