using System;
using System.Globalization;
using _Project.Scripts.Services.Timer.Data;
using _Project.Scripts.Services.Timer.Interfaces;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace _Project.Scripts.Services.Timer
{
  public class GameTimer : IGameTimer
  {
    private readonly IGameTimerService _gameTimerService;
    
    private readonly GameTimerData _data;
    
    private float _currentTime;
    private float _previousTime;
    private uint _targetTime;
    
    private TweenerCore<float, float, FloatOptions> _timer;

    public string UniqueId => 
      _data.UniqueId;

    public event Action<int> OnCount;
    public event Action OnComplete;

    public GameTimer(GameTimerData data, IGameTimerService gameTimerService)
    {
      _gameTimerService = gameTimerService;
      _data = data;
    }

    public void Launch()
    {
      DateTime dateTime = DateTime.Parse(_data.StartTime, CultureInfo.InvariantCulture);
      TimeSpan timeSpend = DateTime.Now.Subtract(dateTime);
      
      double totalSeconds = timeSpend.TotalSeconds;
      
      if (totalSeconds >= _data.RemainTime)
      {    
        _gameTimerService.DestroyTimer(UniqueId);

        OnCount?.Invoke(0);    
        OnComplete?.Invoke();
      }
      else
      {
        _targetTime = _data.RemainTime - (uint) totalSeconds;
        _currentTime = _targetTime + 1;
        
        _timer?.Kill();
        _timer = DOTween.To(() => _currentTime, value => _currentTime = value, 0f, _targetTime)
          .SetEase(Ease.Linear)
          .OnUpdate(TickUpdate)
          .OnComplete(() =>
          {
            _gameTimerService.DestroyTimer(UniqueId);
            OnComplete?.Invoke();
          });
      }
    }

    public uint GetRemain() => 
      (uint)_currentTime;

    public void KillTimerProcess() => 
      _timer?.Kill();

    private void TickUpdate()
    {
      if ((uint) _previousTime != (uint) 
        _currentTime) OnCount?.Invoke((int) _currentTime);

      _previousTime = _currentTime;
    }
  }
}