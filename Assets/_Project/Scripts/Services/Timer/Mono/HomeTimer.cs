using System;
using _Project.Scripts.Services.Timer.Interfaces;

namespace _Project.Scripts.Services.Timer.Mono
{
  public class HomeTimer : IHomeTimer
  {
    private readonly IGameTimerService _gameTimerService;

    private int _timeToWait;
    private Action<bool, int> _onChangeTimeToWait;

    public HomeTimer(IGameTimerService gameTimerService) =>
      _gameTimerService = gameTimerService;

    public void AddListenerOnChangeTimeToWait(Action<bool, int> onChangeTimeToWait)
    {
      _onChangeTimeToWait = onChangeTimeToWait;
      ResetPotentialTime();
    }

    public void CreateTimer(string guid, Action onComplete)
    {
      TimeSpan fromSeconds = TimeSpan.FromSeconds(_timeToWait);
      _gameTimerService.CreateTimer(fromSeconds, guid);
      Launch(guid, () => onComplete?.Invoke());
    }

    public void Launch(string guid, Action onComplete)
    {
      IGameTimer timer = _gameTimerService.GetTimer(guid);
      timer.OnComplete += () => onComplete?.Invoke();
      timer.Launch();
    }
    
    public int AddTime()
    {
      _timeToWait += 60;
      _onChangeTimeToWait?.Invoke(_timeToWait > 0, _timeToWait);
      return _timeToWait;
    }

    public int DecreaseTime()
    {
      _timeToWait -= 60;
      _onChangeTimeToWait?.Invoke(_timeToWait > 0, _timeToWait);
      return _timeToWait = _timeToWait <= 0 ? 0 : _timeToWait;
    }
    
    public void ResetPotentialTime()
    {
      _timeToWait = 0;
      _onChangeTimeToWait?.Invoke(_timeToWait > 0, _timeToWait);
    }
  }
}