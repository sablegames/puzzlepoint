using System;
using _Project.Scripts.Services.Timer.Interfaces;
using UnityEngine;

namespace _Project.Scripts.Services.Timer.Mono
{
  public class TestTimer : MonoBehaviour
  {
    private const string TestTimerKey = "TestTimer.TestTimer";
      
    private IGameTimerService _gameTimerService;
    
    public uint Target;
    
    private string TimerId
    {
      get => PlayerPrefs.GetString(TestTimerKey, "");
      set => PlayerPrefs.SetString(TestTimerKey, value);
    }

    private void Awake()
    {
      _gameTimerService.Initialize();
      
      if (!_gameTimerService.IsTimerExist(TimerId)) 
        return;
      
      IGameTimer gameTimer = _gameTimerService.GetTimer(TimerId);
    
      gameTimer.OnCount += GameTimerOnCount;
      gameTimer.Launch();
    }

    private void LateUpdate()
    {
      if (Input.GetKeyDown(KeyCode.T))
      {
        if (CreateTimer()) 
          return;
      }
      
      if (Input.GetKeyDown(KeyCode.Y)) 
        KillTimer();
    }

    private void KillTimer()
    {
      if (_gameTimerService.IsTimerExist(TimerId))
      {
        _gameTimerService.DestroyTimer(TimerId);
        TimerId = ""; 
        Debug.Log("Destroy timer");
      }
      else
        Debug.Log("Timer not exist");
    }

    private bool CreateTimer()
    {
      if (_gameTimerService.IsTimerExist(TimerId))
      {
        Debug.Log("Timer already created");
        return true;
      }
      
      Debug.Log("Create Timer");

      IGameTimer gameTimer = _gameTimerService.CreateTimer(TimeSpan.FromSeconds(Target));
      TimerId = gameTimer.UniqueId;

      gameTimer.OnCount += GameTimerOnCount;
      gameTimer.Launch();
      
      return false;
    }

    private void GameTimerOnCount(int seconds) => 
      Debug.Log(seconds == 0 ? "Compete" : $"Tick {seconds}");
  }
}