using System;

namespace _Project.Scripts.Services.Timer.Interfaces
{
  public interface IGameTimer
  {
    string UniqueId { get; }
    void Launch();
    uint GetRemain();
    void KillTimerProcess();
    event Action OnComplete;   
    event Action<int> OnCount;
  }
}