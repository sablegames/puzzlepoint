using System;

namespace _Project.Scripts.Services.Timer.Interfaces
{
  public interface IGameTimerService
  {
    void Initialize();
    IGameTimer CreateTimer(TimeSpan duration, string guid = "");
    IGameTimer GetTimer(string id);
    bool IsTimerExist(string id);
    void DestroyTimer(string id);
  }
}