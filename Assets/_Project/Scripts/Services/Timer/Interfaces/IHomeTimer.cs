using System;

namespace _Project.Scripts.Services.Timer.Interfaces
{
  public interface IHomeTimer
  {
    void CreateTimer(string guid, Action onComplete);
    void Launch(string guid, Action onComplete);
    int AddTime();
    int DecreaseTime();
    void ResetPotentialTime();
    void AddListenerOnChangeTimeToWait(Action<bool, int> onChangeTimeToWait);
  }
}