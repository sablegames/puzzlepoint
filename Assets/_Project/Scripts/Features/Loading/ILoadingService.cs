using System;

namespace _Project.Scripts.Features.Loading
{
  public interface ILoadingService
  {
    void Enter(Action onEnd);
    void Exit();
  }
}