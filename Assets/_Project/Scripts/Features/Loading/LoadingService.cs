using System;
using _Project.Scripts.Services.AssetProvider;

namespace _Project.Scripts.Features.Loading
{
  public class LoadingService : ILoadingService
  {
    private readonly ILoadingFactory _loadingFactory;

    public LoadingService(IAssetProvider assetProvider) => 
      _loadingFactory = new LoadingFactory(assetProvider);

    public void Enter(Action onEnd) => 
      _loadingFactory.CreateScreen(() =>
      {
        onEnd?.Invoke();
      });

    public void Exit() => 
      _loadingFactory.ClearScreen();
  }
}