using System;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Features.Loading
{
  public class LoadingMono : MonoBehaviour
  {
    private const float Time = 3f;
    private const float FadeDuration = 1f;
    private const float RotateDuration = 1f;

    public Transform Icon;
    public CanvasGroup CanvasGroup;

    private Tween _tween;
    private Tween _tweenIcon;

    public void Construct(Action onLoad, Action onEnd) =>
      Animate(onLoad, onEnd);

    private void Animate(Action onLoad, Action onEnd)
    {
      AnimateIcon();

      CanvasGroup.alpha = 0;
      CanvasGroup.DOFade(1, FadeDuration)
        .OnComplete(() => onLoad?.Invoke());

      var startValue = 0f;
      _tween = DOTween.To(() => startValue, x => startValue = x, 1f, Time)
        .SetEase(Ease.Linear)
        .OnComplete(() => Hide(onEnd));
    }

    private void AnimateIcon() =>
      _tweenIcon = Icon.DOLocalRotate(Vector3.back * 360, RotateDuration, RotateMode.FastBeyond360)
        .SetLoops(-1, LoopType.Incremental)
        .SetEase(Ease.Linear);

    private void Hide(Action onEnd)
    {
      CanvasGroup.DOFade(0, FadeDuration)
        .OnComplete(() =>
        {
          OnHide();
          onEnd?.Invoke();
        });
    }

    private void OnHide()
    {
      _tween.Kill();
      _tweenIcon.Kill();
      CanvasGroup.DOKill();
    }
  }
}