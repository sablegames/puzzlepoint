using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Timer.Interfaces;

namespace _Project.Scripts.Features.HomeScreen
{
  public interface IHomeViewLogic
  {
    void Construct(IAudioService audioService, IGameTimerService timerController, IHomeTimer homeTimer);
    void Clear();
  }
}