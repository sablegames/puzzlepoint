using System;
using _Project.Scripts.Animation;
using _Project.Scripts.Features.HomeScreen.Mono;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Audio.Executor;
using _Project.Scripts.Services.Timer.Interfaces;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeViewLogic : IHomeViewLogic
  {
    private const float DurationTimerPanel = .25f;
    private const float ColorChangeDuration = .5f;
    private const string TimerKey = "TimerKey";

    private readonly HomeViewMono _homeViewMono;

    private IHomeTimer _homeTimer;
    private IAudioService _audioService;
    private IGameTimerService _gameTimerService;

    private SlidePopup _slidePopup;
    private SlideButtons _slideButtons;
    private FadeCanvasGroup _fadeCanvasGroup;

    public HomeViewLogic(HomeViewMono homeViewMono) =>
      _homeViewMono = homeViewMono;

    public async void Construct(IAudioService audioService, IGameTimerService gameTimerService, IHomeTimer homeTimer)
    {
      _homeTimer = homeTimer;
      _audioService = audioService;
      _gameTimerService = gameTimerService;

      LoadTimers();

      CreateSounds();
      CreateAnimations();
      AddListeners();

      await UniTask.Delay(TimeSpan.FromSeconds(2));

      _slideButtons.Play();
    }

    public void Clear()
    {
      _slideButtons.Clear();
      _homeViewMono.Clear();
      _homeViewMono.TimerPanel.Clear();
    }

    private void LoadTimers()
    {
      for (var i = 0; i < _homeViewMono.ShowTimerButtons.Count; i++)
      {
        string guid = TimerKey + i;
        if (!_gameTimerService.IsTimerExist(guid)) 
          continue;
        
        int index = i;
        _homeTimer.Launch(guid, () => _homeViewMono.ShowTimerImages[index]
          .DOColor(Color.gray, ColorChangeDuration).SetLoops(-1, LoopType.Yoyo));
      }
    }

    private void AddListeners()
    {
      _homeViewMono.TimerPanel.ButtonAdd.gameObject.AddComponent<ButtonHoldHandler>().Construct(() => _homeTimer.AddTime());
      _homeViewMono.TimerPanel.ButtonDecrease.gameObject.AddComponent<ButtonHoldHandler>().Construct(() => _homeTimer.DecreaseTime());
      
      _homeTimer.AddListenerOnChangeTimeToWait(delegate(bool obj, int timeToWait)
      {
        _homeViewMono.TimerPanel.CheckButtonStartAccess(obj);
        _homeViewMono.TimerPanel.SetTimesLeft(timeToWait);
      });

      for (var i = 0; i < _homeViewMono.ShowTimerButtons.Count; i++) 
        AddListenerShowTimerButton(i);
    }

    private void AddListenerShowTimerButton(int i)
    {
      int index = i;
      string guid = TimerKey + i;
      _homeViewMono.ShowTimerButtons[i].onClick.AddListener(() =>
      {
        _slideButtons.Play(ShowTimerPanel);

        if (_gameTimerService.IsTimerExist(guid))
          AddListenerWithCreatedTimer(guid);
        else
          AddListenerWithNoTimer(guid, index);

        _homeViewMono.ClearImage(index);
        _homeViewMono.TimerPanel.SetButtonContainerVisibility(_gameTimerService.IsTimerExist(guid));
      });
    }

    private void AddListenerWithNoTimer(string guid, int index)
    {
      _homeViewMono.TimerPanel.ButtonStart.onClick.AddListener(() =>
      {
        _homeTimer.CreateTimer(guid, () => _homeViewMono.ShowTimerImages[index]
          .DOColor(Color.gray, ColorChangeDuration).SetLoops(-1, LoopType.Yoyo));
        HideTimerPanel();
      });
    }

    private void AddListenerWithCreatedTimer(string guid)
    {
      _homeViewMono.TimerPanel.ButtonContinue.onClick.AddListener(() =>
      {
        _gameTimerService.GetTimer(guid).OnCount -= _homeViewMono.TimerPanel.SetTimesLeft;
        HideTimerPanel();
      });
      _gameTimerService.GetTimer(guid).OnCount += _homeViewMono.TimerPanel.SetTimesLeft;
    }

    private void ShowTimerPanel()
    {
      _slidePopup.Open();
      _fadeCanvasGroup.Appear();
    }

    private void HideTimerPanel()
    {
      _homeViewMono.TimerPanel.RemoveAllListeners();

      _slidePopup.Close();
      _fadeCanvasGroup.Disappear(_homeTimer.ResetPotentialTime, () => _slideButtons.Play());
    }

    private void CreateSounds()
    {
      _homeViewMono.ShowTimerButtons.ForEach(item => new ButtonSound(_audioService, item.gameObject, AudioId.Click));
      new ButtonSound(_audioService, _homeViewMono.TimerPanel.ButtonAdd.gameObject, AudioId.Click);
      new ButtonSound(_audioService, _homeViewMono.TimerPanel.ButtonDecrease.gameObject, AudioId.Click);
      new ButtonSound(_audioService, _homeViewMono.TimerPanel.ButtonStart.gameObject, AudioId.Click);
      new ButtonSound(_audioService, _homeViewMono.TimerPanel.ButtonContinue.gameObject, AudioId.Click);
    }

    private void CreateAnimations()
    {
      _slideButtons = new SlideButtons(_homeViewMono.ShowTimerButtons);
      _slidePopup = new SlidePopup(_homeViewMono.TimerPanel.Container, DurationTimerPanel);
      _fadeCanvasGroup = new FadeCanvasGroup(_homeViewMono.TimerPanel.CanvasGroup, DurationTimerPanel);

      _homeViewMono.ShowTimerButtons.ForEach(item => new ButtonAnimation(item.transform));
      new ButtonAnimation(_homeViewMono.TimerPanel.ButtonAdd.transform);
      new ButtonAnimation(_homeViewMono.TimerPanel.ButtonDecrease.transform);
      new ButtonAnimation(_homeViewMono.TimerPanel.ButtonStart.transform);
      new ButtonAnimation(_homeViewMono.TimerPanel.ButtonContinue.transform);
    }
  }
}