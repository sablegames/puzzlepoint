using _Project.Scripts.Core_Logic.Common;
using _Project.Scripts.Features.HomeScreen.Mono;
using _Project.Scripts.Services.AssetProvider;
using UnityEngine;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeFactory : GameObjectFactory, IHomeFactory
  {
    private const string BaseCanvas = "Canvas";
    private const string HomeViewPath = "Features/HomeScreen/Home";

    private HomeViewMono HomeViewMono { get; set; }
    public IHomeViewLogic HomeViewLogic { get; private set; }

    public HomeFactory(IAssetProvider assetProvider) : base(assetProvider) { }

    public void CreateHomeView()
    {
      ClearHomeView();
      HomeViewMono = Create<HomeViewMono>(HomeViewPath, GameObject.Find(BaseCanvas).transform);
      HomeViewLogic = new HomeViewLogic(HomeViewMono);
    }

    public void ClearHomeView()
    {
      if (!HomeViewMono) return;

      HomeViewLogic.Clear();
      Object.Destroy(HomeViewMono.gameObject);
    }
  }
}