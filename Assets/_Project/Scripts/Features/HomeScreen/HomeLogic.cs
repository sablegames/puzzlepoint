using _Project.Scripts.Services.AssetProvider;
using _Project.Scripts.Services.Audio;
using _Project.Scripts.Services.Audio.Enum;
using _Project.Scripts.Services.Timer.Interfaces;
using _Project.Scripts.Services.Timer.Mono;

namespace _Project.Scripts.Features.HomeScreen
{
  public class HomeLogic
  {
    private IHomeFactory _homeFactory;

    private readonly IAssetProvider _assetProvider;
    private readonly IAudioService _audioService;
    private readonly IGameTimerService _gameTimerService;
    
    private IHomeTimer _homeTimer;
    
    public HomeLogic(IAssetProvider assetProvider, IGameTimerService gameTimerService, IAudioService audioService)
    {
      _assetProvider = assetProvider;
      _audioService = audioService;
      _gameTimerService = gameTimerService;
    }

    public void Enter()
    {
      _gameTimerService.Initialize();
      _homeTimer = new HomeTimer(_gameTimerService);
      _homeFactory = new HomeFactory(_assetProvider);
      
      CreateHomeView();
      
      _audioService.Play(AudioId.Music_OST_Standart);
    }

    public void CloseHomeView() =>
      _homeFactory.ClearHomeView();

    private void CreateHomeView()
    {
      _homeFactory.CreateHomeView();
      _homeFactory.HomeViewLogic.Construct(_audioService, _gameTimerService, _homeTimer);
    }
    
  }
}