using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Project.Scripts.Features.HomeScreen.Mono
{
  public class ButtonHoldHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
  {
    private Action _onHoldAction;
    private CancellationTokenSource _cancellationTokenSource;

    public void Construct(Action onHoldAction) =>
      _onHoldAction = onHoldAction;

    public void OnPointerDown(PointerEventData eventData)
    {
      _cancellationTokenSource = new CancellationTokenSource();
      HoldAddTime(_cancellationTokenSource.Token).Forget();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
      if (_cancellationTokenSource == null)
        return;

      _cancellationTokenSource.Cancel();
      _cancellationTokenSource.Dispose();
      _cancellationTokenSource = null;
    }

    private async UniTaskVoid HoldAddTime(CancellationToken token)
    {
      var interval = 1.0f;
      const float minInterval = 0.1f;
      const float increment = 0.1f;

      try
      {
        while (!token.IsCancellationRequested)
        {
          _onHoldAction?.Invoke();
          await UniTask.Delay((int) (interval * 300), cancellationToken: token);

          if (interval > minInterval)
            interval -= increment;
        }
      }
      catch (OperationCanceledException)
      {
        Debug.Log("Cancel Hold UniTask");
      }
    }
  }
}