using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.HomeScreen.Mono
{
  public class TimerPanel : MonoBehaviour
  {
    public CanvasGroup TimerOptionsCanvasGroup;
    public CanvasGroup ContinueCanvasGroup;

    public CanvasGroup CanvasGroup;
    public RectTransform Container;

    public Button ButtonContinue;
    public Button ButtonAdd;
    public Button ButtonDecrease;
    public Button ButtonStart;

    public TextMeshProUGUI TimerText;

    public void SetTimesLeft(int seconds)
    {
      TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);
      TimerText.text = $"{timeSpan.Hours}:{FormatValue(timeSpan.Minutes)}:{FormatValue(timeSpan.Seconds)}";
    }

    public void SetButtonContainerVisibility(bool isTimerOn, float duration = .25f)
    {
      ContinueCanvasGroup.DOFade(isTimerOn ? 1f : 0f, duration);
      ContinueCanvasGroup.blocksRaycasts = isTimerOn;

      TimerOptionsCanvasGroup.DOFade(isTimerOn ? 0f : 1f, duration);
      TimerOptionsCanvasGroup.blocksRaycasts = !isTimerOn;
    }

    public void Clear()
    {
      ContinueCanvasGroup.DOKill();
      TimerOptionsCanvasGroup.DOKill();
    }
    
    public void CheckButtonStartAccess(bool isAccess) =>
      ButtonStart.interactable = isAccess;

    public void RemoveAllListeners()
    {
      ButtonAdd.onClick.RemoveAllListeners();
      ButtonDecrease.onClick.RemoveAllListeners();
      ButtonStart.onClick.RemoveAllListeners();
    }

    private string FormatValue(int value) =>
      value < 10 ? "0" + value : value.ToString();
  }
}