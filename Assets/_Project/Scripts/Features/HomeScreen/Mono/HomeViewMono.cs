using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Features.HomeScreen.Mono
{
  public class HomeViewMono : MonoBehaviour
  {
    public List<Button> ShowTimerButtons;
    public List<Image> ShowTimerImages;
    
    public TimerPanel TimerPanel;

    public void Clear() =>
      ShowTimerImages.ForEach(item => item.DOKill());

    public void ClearImage(int index)
    {
      ShowTimerImages[index].DOKill();
      ShowTimerImages[index].color = Color.white;
    }
  }
}