namespace _Project.Scripts.Features.Splashscreen
{
  public interface ISplashFactory
  {
    Splash CreateSplash();
    void Clear();
  }
}