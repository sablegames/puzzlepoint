﻿using System;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Animation
{
  public class SlidePopup
  {
    private readonly float _duration;

    private readonly RectTransform _transform;

    public SlidePopup(RectTransform transform, float duration = .5f)
    {
      _duration = duration;
      _transform = transform;
      
      SetupBeforeOpen();
    }

    public void Open(Action onStart = null, Action onEnd = null)
    {
      SetupBeforeOpen();

      _transform.DOAnchorPosY(-_transform.sizeDelta.y, _duration).SetEase(Ease.OutExpo).onComplete += () => onEnd?.Invoke();
      onStart?.Invoke();
    }

    public void Close(Action onStart = null, Action onEnd = null)
    {
      _transform.DOAnchorPosY(0, _duration).SetEase(Ease.OutExpo).onComplete += () => onEnd?.Invoke();
      onStart?.Invoke();
    }

    private void SetupBeforeOpen() => 
      _transform.anchoredPosition = new Vector2( _transform.anchoredPosition.x, 0);
  }
}