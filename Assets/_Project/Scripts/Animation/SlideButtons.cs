using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Animation
{
  public class SlideButtons
  {
    private const float MoveButtonDuration = .5f;
    
    private Sequence _sequence;

    private Action _onStepSequenceComplete;

    public SlideButtons(IReadOnlyList<Button> showTimerButtons) => 
      CreateAnimation(showTimerButtons);

    public void Play(Action onEnd = null)
    {
      if (_sequence.IsPlaying())
        return;

      _onStepSequenceComplete = onEnd;
      _sequence.Play();
    }

    public void Clear() =>
      _sequence.Kill();

    private void CreateAnimation(IReadOnlyList<Button> showTimerButtons)
    {
      _sequence = DOTween.Sequence();
      for (var i = 0; i < showTimerButtons.Count; i++)
      {
        var button = showTimerButtons[i].GetComponent<RectTransform>();
        Vector3 startPos = button.anchoredPosition;
        button.anchoredPosition = new Vector3(-Screen.width, startPos.y, startPos.z);
        _sequence.Append(button.DOAnchorPos(startPos, MoveButtonDuration).SetEase(Ease.OutBack).SetDelay(i * -.2f));
      }

      _sequence.OnStepComplete(() =>
        {
          _onStepSequenceComplete?.Invoke();
          _sequence.Pause();
        })
        .SetLoops(-1, LoopType.Yoyo)
        .Pause();
    }
  }
}