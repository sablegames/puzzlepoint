# Portfolio_1

Архитектура выстроена при помощи стейтов с поддержкой фабрик и необходимых сервисов поставляемых в классы при помощи инъекций.


# Quick start
  1. GameBootstrapper.cs является исходной точкой с которой можно начать и пройти по всему пути логики приложения по порядку. 
	В GameBootstrapper производится инициализация сервисов которым требуется время, за счет показа splash экрана.
  2. Первый переход в следующий state
  
``` 
      void Load()
      {
        _gameStateMachine.Enter<LoadStartupState>();
      }
```

  3. В проекте находятся сервисы рекламы, аналитики, ремоута в Unity Package Manager/My Registries подключаемые с удалённого сервера на DigitalOcean
  4. Так же подключается серия вспомогательных сервисов список которых можно увидеть по пути "_Project/Scripts/Core Logic/Zenject/ServicesRegistration"
  5. Назначение используемых сервисов:
		> AssetProvider - для поиска ресурсов,
		
		> AudioService - для работы со звуковой поддеркой проекта,
		
		> GameSaveSystem - система сохранения данных,
		
		> TimerController - для работы с таймерами,
		
		> LoadingService - загрузка при переходах между экранами
		
  6. В "_Project/Scripts/Core Logic/Zenject/StatesRegistration" имеются все используемые стейты
  7. В "_Project/Scripts/Core Logic/Zenject/ThirdPartyRegistration" находятся рекламный, аналитический и ремоут сервисы